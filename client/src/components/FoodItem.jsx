import React from 'react';

const FoodItem = ({item}) => {
    const [time, setTime] = React.useState(item.expiresIn);
    const [styleSpan, setStyleSpan] = React.useState('badge badge-primary badge-pill');
    const [styleinput, setStyleInput] = React.useState('list-group-item d-flex justify-content-between align-items-center');

    React.useEffect(() => {
        var timerID = setInterval( () => tick(), 1000 );
        return () => {
            clearInterval(timerID);
        };
    });

    const tick = () => {
        time > 0 ? setTime(time - 1) : setTime(0);

        if (time <= 6) {
            setStyleSpan('badge badge-danger badge-pill');
            setStyleInput('list-group-item d-flex justify-content-between align-items-center list-group-item-danger');
        }
    }

    return (
        <li className={styleinput}>
            {item.name}
            <span className={styleSpan}>
                {time}
            </span>
        </li>
    )
}

export default FoodItem;