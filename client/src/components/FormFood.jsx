import React from 'react';

const FormFood = ({addProduct}) => {
    const [name, setName] = React.useState("");
    const [expiresIn, setExpiresIn] = React.useState("");

    const handleSubmit = e => {
        e.preventDefault();
        if (!name || !expiresIn) {
            alert('ERROR! Field Name or Expires is empty!');
            return;
        } else if (expiresIn < 0 || expiresIn > 999) {
            alert('ERROR! Field Expires must be in the range 0 to 999!');
            return;
        }

        addProduct(name, expiresIn);
        setName("");
        setExpiresIn("");
    };

    return (
        <form onSubmit={handleSubmit}>
            <div className="form-group">
                <label htmlFor="nameFood">Product name</label>
                <input 
                    type="text"
                    placeholder="Enter product name..."
                    className="form-control"
                    id="nameFood"
                    value={name} onChange={e => setName(e.target.value)}
                />
            </div>

            <div className="form-group">
                <label htmlFor="expiresIn">Expires In</label>
                <input 
                    type="text"
                    placeholder="Enter expires in..."
                    className="form-control"
                    id="expiresIn"
                    value={expiresIn} onChange={e => setExpiresIn(e.target.value)}
                />
                <small className="form-text text-muted">
                    A number in the range from 1 to 999
                </small>
            </div>

            <button type="submit" className="btn btn-primary">Submit</button>
        </form>
    );
}

export default FormFood;