import React from 'react';
import FoodItem from './FoodItem';
import Loader from './Loader';

const FoodList = ({product}) => {
    return (
        <div className="food-list">
            <h5>Product list:</h5>
            <ul className="food-list list-group text-center">
                {
                    product.length !== 0 ?
                        product.map(item => (
                            <FoodItem item={item} key={item.id} />
                        )) :
                        <Loader />
                }
            </ul>
        </div>
    )
}

export default FoodList;

