import React from 'react';
import './App.css';

import FormFood from './components/FormFood';
import FoodList from './components/FoodList';

const App = () => {
  const baseURL = 'https://us-central1-thefridge-86c35.cloudfunctions.net/api';
  const [product, setProduct] = React.useState([]);

  React.useEffect(() => {
    fetch(`${baseURL}/getFood`)
      .then(res => res.json())
      .then(json => {
        setProduct(json);
      })
  }, []);

  const addProduct = async (name, expiresIn) => {
    await fetch(`${baseURL}/addNewFood`, {
      method: 'POST',
      body: JSON.stringify({ name, expiresIn }),
      headers: {
        'Content-Type': 'application/json'
      }
    });

    fetch(`${baseURL}/getFood`)
      .then(res => res.json())
      .then(json => {
        setProduct(json);
      })
  };

  return (
    <div className="app">
      <div className="container mt-3">
        <div className="col-md-12 text-center">
          <h2>Smart refrigerator</h2>
          <h5>Thefridge test task</h5>
        </div>

        <div className="row mt-5">
          <div className="col-md-4">
            <FormFood addProduct={addProduct} />
          </div>
          <div className="col-md-8">
            <FoodList product={product} />
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
