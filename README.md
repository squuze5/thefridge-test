## Thefridge test task

### URL
```sh
https://thefridge-86c35.firebaseapp.com/
```

----
## BACKEND (NODEJS/express + firebase)
```sh
Base URL
https://us-central1-thefridge-86c35.cloudfunctions.net/api
```

### Routes
- GET     /getAllFood
- POST    /addNewFood

----
## FRONTEND (REACT/bootstrap)
```sh
https://thefridge-86c35.firebaseapp.com/
```