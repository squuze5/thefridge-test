const { db } = require('../util/admin');

exports.getAllFood = (req, res) => {
    db
        .collection('food')
        .get()
        .then(data => {
            let food = [];

            data.forEach(doc => {
                food.push({
                    id: doc.id,
                    name: doc.data().name,
                    expiresIn: doc.data().expiresIn
                })
            })

            return res.status(200).json(food);
        })
        .catch(error => {
            console.error(error);
            return res.status(500).json({
                message: 'Something went wrong!'
            })
        })
}

exports.addNewFood = (req, res) => {
    if (req.body.body === '') {
        return res.status(400).json({
            error: 'Bad request',
            message: 'Must not be empty!'
        })
    }

    const newFood = {
        name: req.body.name,
        expiresIn: req.body.expiresIn
    };

    db
        .collection('food')
        .add(newFood)
        .then(doc => {
            return res.status(200).json({
                message: `Food item with ID ${doc.id} created successfully`
            })
        })
        .catch(error => {
            console.error(error);
            return res.status(500).json({ message: 'Something went wrong!' });
        })
}