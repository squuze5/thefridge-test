const functions = require('firebase-functions');
const express = require('express');
const cors = require('cors');
const app = express();

app.use(cors());

const { getAllFood, addNewFood } = require('./routes/food');

app.get('/getFood', getAllFood);
app.post('/addNewFood', addNewFood);

exports.api = functions.https.onRequest(app);